#![no_std]
#![no_main]
#![feature(abi_avr_interrupt)]

use panic_halt as _;

// If this is changed, then the time formatting also needs to be
// changed.
const NUM_DIGITS: usize = 8;

#[arduino_hal::entry]
fn main() -> ! {
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);
    let mut disp = DisplayPins::new(
        [
            pins.d2.into_output().downgrade(),
            pins.d3.into_output().downgrade(),
            pins.d7.into_output().downgrade(),
            pins.d8.into_output().downgrade(),
            pins.d9.into_output().downgrade(),
            pins.d10.into_output().downgrade(),
            pins.d11.into_output().downgrade(),
            pins.d12.into_output().downgrade(),
        ],
        [
            pins.a2.into_output().downgrade(),
            pins.a4.into_output().downgrade(),
            pins.a1.into_output().downgrade(),
            pins.d4.into_output().downgrade(),
            pins.d5.into_output().downgrade(),
            pins.a0.into_output().downgrade(),
            pins.a3.into_output().downgrade(),
        ],
    );

    let but = pins.a5.into_pull_up_input().downgrade();

    millis_init(dp.TC0);
    unsafe { avr_device::interrupt::enable() };

    let mut serial = arduino_hal::default_serial!(dp, pins, 57600);
    ufmt::uwriteln!(&mut serial, "Start! {}", millis()).unwrap();
    // arduino_hal::delay_ms(1000);
    // ufmt::uwriteln!(&mut serial, "One! {}", millis()).unwrap();

    let mut buf = [' '; NUM_DIGITS];
    let mut total_ms = millis();
    let mut left_ms = 0;
    let mut right_ms = 0;
    loop {
        let ms2 = millis();
        if ms2 - total_ms >= 10 {
            if but.is_high() {
                left_ms += ms2 - total_ms;
            } else {
                right_ms += ms2 - total_ms;
            }
            total_ms = ms2;
            buf = [' '; NUM_DIGITS];
            format_time(&mut buf[0..4], left_ms);
            format_time(&mut buf[4..8], right_ms);
        }
        disp.display_buf(&buf);
    }
}

#[allow(dead_code)]
#[derive(Clone, Copy)]
enum Dig {
    Top,      // A
    TopLeft,  // F
    TopRight, // B
    Mid,      // G
    BotLeft,  // E
    BotRight, // C
    Bot,      // D
    Dot,      // 7
}

#[allow(dead_code)]
#[derive(Clone, Copy)]
enum Seg {
    /// Only digits 1 to 4 inclusive are implemented.
    Dig(usize, Dig),
    // L12,
    // L3,
}

type DynamicPin = arduino_hal::port::Pin<arduino_hal::port::mode::Output>;

struct DisplayPins {
    digit_select: [DynamicPin; NUM_DIGITS],
    segments: [DynamicPin; 7],
}

impl DisplayPins {
    fn new(digit_select: [DynamicPin; NUM_DIGITS], segments: [DynamicPin; 7]) -> Self {
        Self {
            digit_select,
            segments,
        }
    }

    fn set_digit_high(&mut self, idx: usize) {
        if idx >= 1 && idx <= self.digit_select.len() {
            self.digit_select[idx - 1].set_high();
        }
    }

    fn set_digit_segment_low(&mut self, dig: Dig) {
        match dig {
            Dig::Top => self.segments[0].set_low(),      // A
            Dig::TopRight => self.segments[1].set_low(), // B
            Dig::BotRight => self.segments[2].set_low(), // C
            Dig::Bot => self.segments[3].set_low(),      // D
            Dig::BotLeft => self.segments[4].set_low(),  // E
            Dig::TopLeft => self.segments[5].set_low(),  // F
            Dig::Mid => self.segments[6].set_low(),      // G
            Dig::Dot => {}
        }
    }

    // Clear the display by setting all the digit select pins to low
    // and all the segment pins to high;
    fn clear(&mut self) {
        for p in self.digit_select.iter_mut() {
            p.set_low();
        }
        for p in &mut self.segments {
            p.set_high();
        }
    }

    fn display_buf(&mut self, buf: &[char; NUM_DIGITS]) {
        for (idx, ch) in buf.iter().enumerate() {
            self.clear();
            if let Some(digs) = digit_segments(*ch) {
                for dig in digs {
                    self.set_digit_segment_low(*dig);
                    self.set_digit_high(idx + 1);
                }
            }
        }
    }
}

fn digit_segments(c: char) -> Option<&'static [Dig]> {
    match c {
        ' ' => Some(&[]),
        '0' => Some(&[
            Dig::Top,
            Dig::TopRight,
            Dig::TopLeft,
            Dig::BotLeft,
            Dig::BotRight,
            Dig::Bot,
        ]),
        '1' => Some(&[Dig::TopRight, Dig::BotRight]),
        '2' => Some(&[Dig::Top, Dig::TopRight, Dig::Mid, Dig::BotLeft, Dig::Bot]),
        '3' => Some(&[Dig::Top, Dig::TopRight, Dig::Mid, Dig::BotRight, Dig::Bot]),
        '4' => Some(&[Dig::TopLeft, Dig::Mid, Dig::TopRight, Dig::BotRight]),
        '5' => Some(&[Dig::Top, Dig::TopLeft, Dig::Mid, Dig::BotRight, Dig::Bot]),
        '6' => Some(&[
            Dig::Top,
            Dig::TopLeft,
            Dig::Mid,
            Dig::BotLeft,
            Dig::BotRight,
            Dig::Bot,
        ]),
        '7' => Some(&[Dig::Top, Dig::TopRight, Dig::BotRight]),
        '8' => Some(&[
            Dig::Top,
            Dig::TopLeft,
            Dig::TopRight,
            Dig::Mid,
            Dig::BotLeft,
            Dig::BotRight,
            Dig::Bot,
        ]),
        '9' => Some(&[
            Dig::Top,
            Dig::TopLeft,
            Dig::TopRight,
            Dig::Mid,
            Dig::BotRight,
            Dig::Bot,
        ]),
        _ => None,
    }
}

fn format_time(buf: &mut [char], ms: u32) {
    let sec = (ms / 1000) % 60;
    let min = (ms / 1000 / 60) % 100;
    buf[0] = core::char::from_digit(min / 10 % 10, 10).unwrap();
    buf[1] = core::char::from_digit(min % 10, 10).unwrap();
    buf[2] = core::char::from_digit(sec / 10 % 10, 10).unwrap();
    buf[3] = core::char::from_digit(sec % 10, 10).unwrap();
}

static MILLIS_COUNTER: avr_device::interrupt::Mutex<core::cell::Cell<u32>> =
    avr_device::interrupt::Mutex::new(core::cell::Cell::new(0));

fn millis() -> u32 {
    avr_device::interrupt::free(|cs| MILLIS_COUNTER.borrow(cs).get())
}

// https://blog.rahix.de/005-avr-hal-millis/
const PRESCALER: u32 = 1024;
const TIMER_COUNTS: u32 = 250;
const MILLIS_INCREMENT: u32 = PRESCALER * TIMER_COUNTS / 16000;

fn millis_init(tc0: arduino_hal::pac::TC0) {
    tc0.tccr0a.write(|w| w.wgm0().ctc());
    tc0.ocr0a.write(|w| w.bits(TIMER_COUNTS as u8));
    tc0.tccr0b.write(|w| match PRESCALER {
        8 => w.cs0().prescale_8(),
        64 => w.cs0().prescale_64(),
        256 => w.cs0().prescale_256(),
        1024 => w.cs0().prescale_1024(),
        _ => panic!("unknown prescaler value"),
    });
    tc0.timsk0.write(|w| w.ocie0a().set_bit());
    avr_device::interrupt::free(|cs| {
        MILLIS_COUNTER.borrow(cs).set(0);
    });
}

#[avr_device::interrupt(atmega328p)]
fn TIMER0_COMPA() {
    avr_device::interrupt::free(|cs| {
        let counter_cell = MILLIS_COUNTER.borrow(cs);
        let counter = counter_cell.get();
        counter_cell.set(counter + MILLIS_INCREMENT);
    })
}
